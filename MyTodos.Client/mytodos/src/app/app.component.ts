import { Component } from '@angular/core';
import { UserService } from './user/user.service';
import { TodoService } from './todo/todo.service';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService, TodoService]
})
export class AppComponent {
  constructor(private _authService: AuthService) { }

  pageTitle: string = 'Todo Manager v1.0.0';

  public signout(): void {
    this._authService.signout();
 }
}
