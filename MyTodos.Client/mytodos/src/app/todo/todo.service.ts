import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

import { User } from '../user/user.model';
import { Todo } from './todo.model';
import { Adal5Service } from 'adal-angular5';

@Injectable()
export class TodoService {
  private _rootUrl  = environment.apiRootUrl + 'api/Todo/';
  private options = this.prepareOptions();
  constructor(private _http: HttpClient, private adal5Service: Adal5Service) { }

  addTodo(todo: Todo): Observable<any> {
    const body  = JSON.stringify(todo);
    return this._http.post(this._rootUrl, body,  this.options);
  }
  getTodos(page: number): Observable<Todo[]> {
    return this._http.get<Todo[]>(this._rootUrl + '?page=' + page);
  }
  deleteTodo(id: number): Observable<any> {
    return this._http.delete(this._rootUrl + id, this.options);
  }
  updateTodo(todo: Todo): Observable<any> {
    const body  = JSON.stringify(todo);
    return this._http.put(this._rootUrl, body, this.options);
  }
  private prepareOptions(): Object {
    let headers = new HttpHeaders();
           headers = headers
               .set('Content-Type', 'application/json')
               .set('Authorization', `Bearer ${this.adal5Service.userInfo.token}`);
           return { headers };
   }
}
