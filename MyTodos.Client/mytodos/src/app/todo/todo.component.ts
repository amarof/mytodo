import {Input, Component, OnInit, TemplateRef } from '@angular/core';
import { Todo } from './todo.model';
import { PopoverDirective, BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TodoService } from './todo.service';
import { FormGroup, FormControl } from '@angular/forms/src/model';
import { User } from '../user/user.model';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  todos: Todo[];
  totalItems = 0;
  currentPage = 1;
  todoIdTeDelete = 0;
  popOverToCancel: PopoverDirective;
  modalRef: BsModalRef;
  currentTodoToUpdate: Todo;
  users: User[];
  dismissible = true;
  constructor(private _service: TodoService,
     private _userService: UserService,
     private modalService: BsModalService) { }

  ngOnInit() {
    this.getTodos();
    this.getUsers();
  }
  getUsers() {
    this._userService.getUsers(this.currentPage)
    .subscribe(res => {
        this.users = res['data'];
        this.totalItems = res['totalRecords'];
     });
  }
  getTodos() {
    this._service.getTodos(this.currentPage)
    .subscribe(res => {
        this.todos = res['data'];
        this.totalItems = res['totalRecords'];
     });
  }

  addTodo(form: FormGroup) {    
    const todo:Todo = new Todo();
    todo.description = form.controls['description'].value;
    todo.status = 0;
    todo.userId = form.controls['user'].value;
    this._service.addTodo(todo)
    .subscribe(data => {
      this.getTodos();
      form.reset();
    });
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.getTodos();
  }

  setTodoIdToDelete(todo: Todo, pop: PopoverDirective) {
    this.popOverToCancel = pop;
    this.todoIdTeDelete = todo.id;
  }
  deleteTodo() {
    this._service.deleteTodo(this.todoIdTeDelete)
    .subscribe(data => {
      this.getTodos();
    });
  }
  updateTodo() {    
    this._service.updateTodo(this.currentTodoToUpdate)
    .subscribe(data => {
      this.getTodos();
      this.modalRef.hide();
    });
  }  
  cancelDeleteTodo() {
    this.popOverToCancel.hide();
  }

  closeError(form: FormGroup) {
    form.reset();
  }

  openModal(todo: Todo, template: TemplateRef<any>) {
    const tmp :Todo = new Todo();
    tmp.id = todo.id;
    tmp.description = todo.description;
    tmp.status = todo.status;
    tmp.userId = todo.userId;
    tmp.user = todo.user;
    this.currentTodoToUpdate = tmp;    
    this.modalRef = this.modalService.show(template);
  }  
}
