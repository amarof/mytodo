import { User } from "../user/user.model";

export class Todo {
    id: number;
    description: string;
    status : number;
    statusName : string;
    userId : number;
    user : User
}