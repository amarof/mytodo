import { Adal5HTTPService, Adal5Service } from 'adal-angular5';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthService {

private _user= null;
private _config = {
   tenant: '0d333e5e-57e1-4d64-b1e3-07f34db59e96',
   clientId: '9191a6f1-c67b-4c05-b4fa-5d4069558c5a',
   redirectUri: 'http://localhost:4200/auth-callback',
   postLogoutRedirectUri: 'http://localhost:4200'
}

constructor(private _adal: Adal5Service) {
   this._adal.init(this._config);
}
public isLoggedIn(): boolean {
   return this._adal.userInfo.authenticated;
}
public signout(): void {
   this._adal.logOut();
}
public startAuthentication(): any {
   this._adal.login();
}
public getName(): string {
   return this._user.profile.name;
}
public completeAuthentication(): void {
   this._adal.handleWindowCallback();
   this._adal.getUser().subscribe(user => {
   this._user = user;
   console.log(this._adal.userInfo);
   const expireIn = user.profile.exp - new Date().getTime();
});
}
}
