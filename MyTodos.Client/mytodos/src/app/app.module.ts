import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PaginationModule, PopoverModule, ModalModule, AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { TodoComponent } from './todo/todo.component';
import { AuthGuardService } from './auth-guard.service';
import { Adal5HTTPService, Adal5Service } from 'adal-angular5';
import { AuthService } from './auth.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    TodoComponent,
    AuthCallbackComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forRoot([
      { path: 'users', component: UserComponent , canActivate: [AuthGuardService]},
      { path: 'todos', component: TodoComponent, canActivate: [AuthGuardService] },
      { path : 'auth-callback', component: AuthCallbackComponent }
  ]),
  ],
  providers: [AuthGuardService, AuthService, Adal5Service,
    { provide: Adal5HTTPService, useFactory: Adal5HTTPService.factory, deps: [HttpClient, Adal5Service]}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
