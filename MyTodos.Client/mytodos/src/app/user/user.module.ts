import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserService } from './user.service';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'users', component: UserComponent }
    ]),
  ],
  declarations: [
    UserComponent
  ],
  providers:[
    UserService
  ]

})
export class UserModule { }
