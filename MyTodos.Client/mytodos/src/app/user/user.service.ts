import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

import { User } from './user.model';
import { Adal5Service } from 'adal-angular5';

@Injectable()
export class UserService {
  private _rootUrl  = environment.apiRootUrl + 'api/User/';
  private options = this.prepareOptions();
  constructor(private _http: HttpClient, private adal5Service: Adal5Service) { }

  addUser(user: User): Observable<any> {
    const body  = JSON.stringify(user);
    return this._http.post(this._rootUrl, body,  this.options);
  }
  getUsers(page: number): Observable<User[]> {
    const url = this._rootUrl + '?page=' + page;
    return this._http.get<User[]>(url, this.options);
  }
  deleteUser(id: number): Observable<any> {
    return this._http.delete(this._rootUrl + id, this.options);
  }
  updateUser(id: number, name: string): Observable<any> {
    const body  = JSON.stringify({'Name' : name});
    return this._http.put(this._rootUrl + id, body, this.options);
  }
  private prepareOptions(): Object {
    let headers = new HttpHeaders();
           headers = headers
               .set('Content-Type', 'application/json')
               .set('Authorization', `Bearer ${this.adal5Service.userInfo.token}`);
           return { headers };
   }
}
