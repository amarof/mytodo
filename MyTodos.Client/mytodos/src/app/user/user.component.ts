import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { User } from './user.model';
import { UserService } from './user.service';
import { FormGroup, FormControl } from '@angular/forms/src/model';
import { PopoverDirective } from 'ngx-bootstrap/popover/popover.directive';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @ViewChild('userName', {read: ElementRef})userNameRef: ElementRef;
  model: User = new User();
  users: User[];
  totalItems = 0;
  currentPage = 1;
  userIdTeDelete = 0;
  popOverToCancel: PopoverDirective;
  modalRef: BsModalRef;
  currentUserToUpdate: User;
  constructor( private _service: UserService, private modalService: BsModalService) {
   }

  ngOnInit() {
    this.getUsers();
  }
  getUsers() {
    this._service.getUsers(this.currentPage)
    .subscribe(res => {
        this.users = res['data'];
        this.totalItems = res['totalRecords'];
     });
  }
  /**
   * add user
   */
  addUser(form: FormGroup) {
    this._service.addUser(this.model)
    .subscribe(data => {
      this.getUsers();
      form.reset();
      this.userNameRef.nativeElement.focus();
    });
  }
  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.getUsers();
  }
  setUserIdToDelete(user: User, pop: PopoverDirective) {
    this.popOverToCancel = pop;
    this.userIdTeDelete = user.id;
  }
  deleteUser() {
    this._service.deleteUser(this.userIdTeDelete)
    .subscribe(data => {
      console.log('User with id:' +  this.userIdTeDelete + ' successfully deleted');
      this.getUsers();
    });
  }
  cancelDeleteUser() {
    this.popOverToCancel.hide();
  }

  openModal(user: User, template: TemplateRef<any>) {
    this.currentUserToUpdate = user;
    console.log(this.currentUserToUpdate);
    this.modalRef = this.modalService.show(template);
  }
  updateUser(form: FormGroup) {
    const userName = form.controls['userNameToUpdate'].value;
    this._service.updateUser(this.currentUserToUpdate.id, userName)
    .subscribe(data => {
      this.getUsers();
      this.modalRef.hide();
    });
  }
  closeError(form: FormGroup) {
    form.reset();
  }
}
