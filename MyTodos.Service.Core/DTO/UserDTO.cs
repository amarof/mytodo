﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Service.DTO
{
    public class UserDTO :EntityDTO
    {        
        public string Name { get; set; }        
    }
}
