﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTodos.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using MyTodos.Service.DTO;
using System.Collections.Generic;

namespace MyTodos.Service.Test
{
    [TestClass]
    public class MyTodoServiceTest
    {
        [TestMethod]
        public void CreateUser()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }

                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                }

                // Use a separate instance of the context to verify correct data was saved to database
                using (var context = new TodoContext(options))
                {
                    User user = context.Users.FirstOrDefault(u => u.Name == "jalal");
                    Assert.IsNotNull(user);
                }
            }
            finally
            {
                connection.Close();
            }
        }
        [TestMethod]
        public void GetAllUsers()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    service.CreateUser("jamila");
                    service.CreateUser("omar");
                    service.CreateUser("sara");
                    service.CreateUser("lina");
                    service.CreateUser("jalal1");
                    service.CreateUser("jamila1");
                    service.CreateUser("omar1");
                    service.CreateUser("sara1");
                    service.CreateUser("lina1");
                    service.CreateUser("jalal1");
                    service.CreateUser("jamila1");
                    service.CreateUser("omar1");
                    service.CreateUser("sara1");
                    service.CreateUser("lina1");
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    List<UserDTO> users = service.GetUsers(1);
                    Assert.AreEqual(users.Count, 10);
                    UserDTO user1 = users.Find(u => u.Name == "lina1");
                    Assert.IsNotNull(user1);
                    UserDTO user2 = users.Find(u => u.Name == "sara1");
                    Assert.IsNotNull(user2);
                    UserDTO user3 = users.Find(u => u.Name == "omar1");
                    Assert.IsNotNull(user3);
                    UserDTO user4 = users.Find(u => u.Name == "jamila1");
                    Assert.IsNotNull(user4);
                    UserDTO user5 = users.Find(u => u.Name == "jalal1");
                    Assert.IsNotNull(user5);
                    users = service.GetUsers(2);
                    Assert.AreEqual(users.Count, 5);

                }

            }
            finally
            {
                connection.Close();
            }
        }
        [TestMethod]
        public void GetUserById()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    UserDTO user = service.GetUserById(1);
                    Assert.IsNotNull(user);
                }

            }
            finally
            {
                connection.Close();
            }
        }
        [TestMethod]
        public void UpdateUser()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    service.UpdateUser(1, "jamila");
                    UserDTO user = service.GetUserById(1);
                    Assert.AreEqual(user.Name, "jamila");
                }

            }
            finally
            {
                connection.Close();
            }
        }
        [TestMethod]
        public void DeleteUser()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    service.DeleteUserById(1);
                    UserDTO user = service.GetUserById(1);
                    Assert.IsNull(user);
                }
            }
            finally
            {
                connection.Close();
            }
        }
        [TestMethod]
        public void CreateTodo()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }

                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    TodoDTO dto = new TodoDTO {
                        Description = "test",
                        Status = Status.Open,
                        UserId = 1
                    };
                    service.CreatTodo(dto);
                }

                // Use a separate instance of the context to verify correct data was saved to database
                using (var context = new TodoContext(options))
                {
                    Todo t = context.Todos.Include(x=>x.User).FirstOrDefault(x => x.ID == 1);
                    Assert.IsNotNull(t);
                    Assert.AreEqual(t.Description, "test");
                    Assert.AreEqual(t.Status, Status.Open);
                    Assert.AreEqual(t.User.Name, "jalal");

                }
            }
            finally
            {
                connection.Close();
            }
        }

        [TestMethod]
        public void GetAllTodos()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    TodoDTO d1 = new TodoDTO
                    {
                        Description = "test1",
                        Status = Status.Open,
                        UserId = 1
                    };
                    TodoDTO d2 = new TodoDTO
                    {
                        Description = "test2",
                        Status = Status.Open,
                        UserId = 1
                    };
                    TodoDTO d3 = new TodoDTO
                    {
                        Description = "test3",
                        Status = Status.Open,
                        UserId = 1
                    };
                    service.CreatTodo(d1);
                    service.CreatTodo(d2);
                    service.CreatTodo(d3);
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    List<TodoDTO> todos = service.GetTodos(1);
                    Assert.AreEqual(todos.Count, 3);
                    TodoDTO t1 = todos.Find(x=>x.Description == "test1");
                    Assert.IsNotNull(t1);
                    TodoDTO t2 = todos.Find(x=>x.Description == "test2");
                    Assert.IsNotNull(t2);  
                    TodoDTO t3 = todos.Find(x=>x.Description == "test3");
                    Assert.IsNotNull(t3);
                    todos = service.GetTodos(2);
                    Assert.AreEqual(todos.Count, 0);

                }

            }
            finally
            {
                connection.Close();
            }
        }

        [TestMethod]
        public void GetTodoById()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    TodoDTO dto = new TodoDTO
                    {
                        Description = "test",
                        Status = Status.Open,
                        UserId = 1
                    };
                    service.CreatTodo(dto);
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    TodoDTO t = service.GetTodoById(1);
                    Assert.IsNotNull(t);
                }

            }
            finally
            {
                connection.Close();
            }
        }

        [TestMethod]
        public void UpdateTodo()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    service.CreateUser("omar");
                    TodoDTO dto = new TodoDTO
                    {
                        Description = "test",
                        Status = Status.Open,
                        UserId = 1
                    };
                    service.CreatTodo(dto);
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    TodoDTO dto = new TodoDTO
                    {
                        ID = 1,
                        Description = "xxxx",
                        Status = Status.Completed,
                        UserId = 2
                    };
                    service.UpdateTodo(dto);
                    TodoDTO d = service.GetTodoById(1);
                    Assert.AreEqual(d.Description, "xxxx");
                    Assert.AreEqual(d.Status, Status.Completed);
                    Assert.AreEqual(d.UserId, 2);
                }

            }
            finally
            {
                connection.Close();
            }
        }

        [TestMethod]
        public void DeleteTodo()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<TodoContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database
                using (var context = new TodoContext(options))
                {
                    context.Database.EnsureCreated();
                }
                using (var context = new TodoContext(options))
                {
                    var service = new MyTodoService(context);
                    service.CreateUser("jalal");
                    TodoDTO dto = new TodoDTO
                    {
                        Description = "test",
                        Status = Status.Open,
                        UserId = 1
                    };
                    service.CreatTodo(dto);
                }
                // Run the test against one instance of the context
                using (var context = new TodoContext(options))
                {
                    MyTodoService service = new MyTodoService(context);
                    service.DeleteTodoById(1);
                    TodoDTO t = service.GetTodoById(1);
                    Assert.IsNull(t);
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
