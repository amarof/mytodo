﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Model
{
    public class Todo : Entity
    {        
        public string Description { get; set; }
        public Status Status { get; set; }

        public  int UserId { get; set; }
        public virtual User User { get; set; }
    }

    public enum Status
    {
        Open,
        Completed
    }
}
