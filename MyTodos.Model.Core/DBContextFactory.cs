﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Model
{
    public class DBContextFactory : IDesignTimeDbContextFactory<TodoContext>
    {
        public TodoContext CreateDbContext(string[] options)
        {
            var builder = new DbContextOptionsBuilder<TodoContext>();
            builder.UseSqlServer("Server=.;Database=tododb;Trusted_Connection=True;MultipleActiveResultSets=true",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(TodoContext).GetTypeInfo().Assembly.GetName().Name));
            return new TodoContext(builder.Options);
        }
    }
}
