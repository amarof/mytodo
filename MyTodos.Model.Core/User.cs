﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Model
{
    public class User : Entity
    {        
        public string Name { get; set; }

        public ICollection <Todo> Todos { get; set; }
    }
}
