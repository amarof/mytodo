﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using MyTodos.Model;
using MyTodos.Service.DTO;
namespace MyTodos.Service
{
    public class MyTodoService : ITodoService
    {
        TodoContext _dbContext;
        IMapper _mapper;
        int recordsByPage = 10;
        public MyTodoService(DbContext dbContext)
        {
            _dbContext = (TodoContext) dbContext;
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<Todo, TodoDTO>();
            });
            _mapper = config.CreateMapper();
        }
        #region User
        //Create (C)
        public void CreateUser(string name)
        {
            User user = new User
            {
                Name = name
            };
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();

        }
        //Read (R)
        public UserDTO GetUserById(int id)
        {
            User user = _dbContext.Users.Where(u => u.ID == id).FirstOrDefault<User>();
            UserDTO dto = _mapper.Map<User, UserDTO>(user);
            return dto;
        }
        //Read (ALL)

        public List<UserDTO> GetUsers(int page)
        {
            page = page - 1;
            List<User> users = _dbContext.Users.OrderByDescending(x => x.ID).Skip(page * recordsByPage).Take(recordsByPage).ToList<User>();
            IList<UserDTO> userDtos = _mapper.Map<IList<User>, IList<UserDTO>>(users);
            return userDtos.ToList<UserDTO>();
        }
        //Update (U)
        public void UpdateUser(int id, string name)
        {
            User user = _dbContext.Users.FirstOrDefault<User>(u => u.ID == id);
            user.Name = name;
            _dbContext.SaveChanges();
        }
        //Delete (D)
        public void DeleteUserById(int id)
        {
            User user = _dbContext.Users.FirstOrDefault<User>(u => u.ID == id);
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }

        public int GetTotalUsersCount()
        {
            return _dbContext.Users.Count();
        }
        #endregion
        #region Todo
        public void CreatTodo(TodoDTO todo)
        {
            Todo newTodo = new Todo
            {
                Description = todo.Description,
                Status = todo.Status,
                UserId = todo.UserId
            };
            _dbContext.Todos.Add(newTodo);
            _dbContext.SaveChanges();
        }

        public TodoDTO GetTodoById(int id)
        {
            Todo todo = _dbContext.Todos.Where(t => t.ID == id).FirstOrDefault<Todo>();
            TodoDTO dto = _mapper.Map<Todo, TodoDTO>(todo);
            return dto;
        }

        public List<TodoDTO> GetTodos(int page)
        {
            page = page - 1;
            List<Todo> todos = _dbContext.Todos.OrderByDescending(x => x.ID).Skip(page * recordsByPage).Take(recordsByPage)
                .Include(todo => todo.User).ToList<Todo>();
            IList<TodoDTO> todoDTO = _mapper.Map<IList<Todo>, IList<TodoDTO>>(todos);
            return todoDTO.ToList<TodoDTO>();
        }

        public void UpdateTodo(TodoDTO todo)
        {
            Todo t = _dbContext.Todos.FirstOrDefault<Todo>(u => u.ID == todo.ID);
            t.Description = todo.Description;
            t.UserId = todo.UserId;
            t.Status = todo.Status;
            _dbContext.SaveChanges();
        }

        public void DeleteTodoById(int id)
        {
            Todo t = _dbContext.Todos.FirstOrDefault<Todo>(u => u.ID == id);
            _dbContext.Todos.Remove(t);
            _dbContext.SaveChanges();
        }

        public int GetTotalTodosCount()
        {
            return _dbContext.Todos.Count();
        }
        #endregion

    }
}
