﻿using MyTodos.Service.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Service
{
    public interface ITodoService
    {
        #region User
        void CreateUser(string name);
        UserDTO GetUserById(int id);
        List<UserDTO> GetUsers(int page);
        void UpdateUser(int id, string name);
        void DeleteUserById(int id);
        int GetTotalUsersCount();
        #endregion
        #region Todo
        void CreatTodo(TodoDTO todo);
        TodoDTO GetTodoById(int id);
        List<TodoDTO> GetTodos(int page);
        void UpdateTodo(TodoDTO todo);
        void DeleteTodoById(int id);
        int GetTotalTodosCount();
        #endregion



    }
}
