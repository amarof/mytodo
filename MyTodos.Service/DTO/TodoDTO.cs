﻿using MyTodos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTodos.Service.DTO
{
    public class TodoDTO : EntityDTO
    {
        public string Description { get; set; }
        public Status Status { get; set; }
        public string StatusName
        {
            get
            {
                if (Status == Status.Open)
                {
                    return "Open";
                }
                else
                {
                    return "Completed";
                }
            }
        }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
    }
}