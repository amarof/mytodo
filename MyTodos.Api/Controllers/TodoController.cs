﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyTodos.Service;
using MyTodos.Api.Model;
using MyTodos.Service.DTO;
using Microsoft.AspNetCore.Authorization;

namespace MyTodos.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Todo")]
    public class TodoController : Controller
    {
        ITodoService _service;

        public TodoController(ITodoService service)
        {
            _service = service;
        }
        // GET: api/Todo
        /// <summary>
        /// Get the first page of all todos
        /// the page parameter should be passed in the query
        /// </summary>                
        [HttpGet]
        public AppResponse Get()
        {
            int page = Int32.Parse(this.Request.Query["page"].FirstOrDefault());
            IEnumerable<TodoDTO> todos = _service.GetTodos(page);
            AppResponse response = new AppResponse
            {
                Data = todos,
                TotalRecords = _service.GetTotalTodosCount(),
                Success = true
            };
            return response;
        }
        /// <summary>
        /// Get Todo by Id        
        /// </summary> 
        // GET: api/Todo/5
        [HttpGet("{id}")]
        public AppResponse Get(int id)
        {
            TodoDTO todo = _service.GetTodoById(id);
            AppResponse response = new AppResponse
            {
                Data = todo,
                Success = true
            };
            return response;
        }

        // POST: api/Todo

        [HttpPost]
        /// <summary>
        ///Create new Todo     
        /// </summary> 
        public AppResponse Post([FromBody]TodoDTO todo)
        {
            _service.CreatTodo(todo);
            AppResponse response = new AppResponse
            {
                Success = true
            };
            return response;
        }

        // PUT: api/Todo/
        /// <summary>
        /// Update existing todo
        /// </summary> 
        [HttpPut]
        public AppResponse Put([FromBody]TodoDTO todo)
        {
            _service.UpdateTodo(todo);
            AppResponse response = new AppResponse
            {
                Success = true
            };
            return response;
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// delete Todo by Id        
        /// </summary> 
        [HttpDelete("{id}")]
        public AppResponse Delete(int id)
        {
            _service.DeleteTodoById(id);
            AppResponse response = new AppResponse
            {
                Success = true
            };
            return response;
        }
    }
}
