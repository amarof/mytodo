﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyTodos.Service;
using MyTodos.Model;
using MyTodos.Api.Model;
using MyTodos.Service.DTO;
using Microsoft.AspNetCore.Authorization;

namespace MyTodos.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        ITodoService _service;

        public UserController(ITodoService service)
        {
            _service = service;
        }
        // GET: api/User
        [HttpGet]
        public AppResponse Get()
        {
            int  page = Int32.Parse(this.Request.Query["page"].FirstOrDefault());
            IEnumerable<UserDTO> users = _service.GetUsers(page);
            AppResponse response = new AppResponse {
                Data = users,
                TotalRecords = _service.GetTotalUsersCount(),
                Success = true
            };
            return response;
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public AppResponse Get(int id)
        {
            UserDTO user = _service.GetUserById(id);
            AppResponse response = new AppResponse
            {
                Data = user,
                Success = true
            };
            return response;
        }
        
        // POST: api/User
        [HttpPost]
        public AppResponse Post([FromBody]UserDTO user)
        {
            _service.CreateUser(user.Name);
            AppResponse response = new AppResponse
            {                
                Success = true
            };
            return response;
        }
        
        // PUT: api/User/5
        [HttpPut("{id}")]
        public AppResponse Put(int id, [FromBody]UserDTO user)
        {
            _service.UpdateUser(id, user.Name);
            AppResponse response = new AppResponse
            {
                Success = true
            };
            return response;
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public AppResponse Delete(int id)
        {
            _service.DeleteUserById(id);
            AppResponse response = new AppResponse
            {
                Success = true
            };
            return response;
        }
    }
}
