﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTodos.Api.Model
{
    public class AppResponse
    {
        public object Data { get; set; }
        public int TotalRecords { get; set; }
        public Boolean Success { get; set; } = false;
        public string Error { get; set; } = string.Empty;

    }
}
